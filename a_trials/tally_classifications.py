import json, os

def merge_dictionaries(dict1, dict2):
    if dict1.keys() == dict2.keys():
        for key in dict1:
            dict1[key] += dict2[key]

        return dict1
    else:
        raise

def tally_bulk(file_prefix, gens, milestone):
    i = 0
    p1_strats = ['DWOL', 'DWL', 'CWOL', 'CWL', 'All L', 'Unclassified']
    p2_strats = ['All E', 'All C', 'Unclassified']

    p1_tallies = {i:{} for i in p1_strats}
    p2_tallies = {i:{} for i in p2_strats}

    while os.path.isdir('{}trial{}'.format(file_prefix, i)):
        p1_tallies_trial, p2_tallies_trial = tally(file_prefix + 'trial{}/'.format(i), gens, milestone, True)
        for key in p1_tallies:
            p1_tallies = merge_dictionaries(p1_tallies[key], p1_tallies_trial[key])

        for key in p2_tallies:
            p2_tallies = merge_dictionaries(p2_tallies[key], p2_tallies_trial[key])

    with open('{}player1_tally.json'.format(file_prefix), 'w+') as outfile:
        outfile.write(json.dumps(p1_tallies))

    with open('{}player2_tally.json'.format(file_prefix), 'w+') as outfile:
        outfile.write(json.dumps(p2_tallies))



def tally(file_prefix, gens, milestone, return_data=False):
    i = 0
    p1_strats = ['DWOL', 'DWL', 'CWOL', 'CWL', 'All L', 'Unclassified']
    p2_strats = ['All E', 'All C', 'Unclassified']

    p1_tallies = {i:{} for i in p1_strats}
    p2_tallies = {i:{} for i in p2_strats}

    while i <= gens:

        if os.path.isfile('{}classification/gen{}_player1.json'.format(file_prefix, i)):
            player1_infile = open('{}classification/gen{}_player1.json'.format(file_prefix, i), 'r')
            player2_infile = open('{}classification/gen{}_player2.json'.format(file_prefix, i), 'r')
            player1s = json.load(player1_infile)
            player2s = json.load(player2_infile)

            for strat in p1_strats:
                p1_tallies[strat][i] = 0

            for strat in p2_strats:
                p2_tallies[strat][i] = 0

            for player1 in player1s:
                if player1[0] is None:
                    p1_tallies['Unclassified'][i] += player1[1]
                else:
                    p1_tallies[player1[0]][i] += player1[1]


            for player2 in player2s:
                if player2[0] is None:
                    p2_tallies['Unclassified'][i] += player2[1]
                else:
                    p2_tallies[player2[0]][i] += player2[1]



        i += milestone

    with open('{}classification/player1_tally.json'.format(file_prefix), 'w+') as outfile:
        outfile.write(json.dumps(p1_tallies))

    with open('{}classification/player2_tally.json'.format(file_prefix), 'w+') as outfile:
        outfile.write(json.dumps(p2_tallies))

    if return_data:
        return p1_tallies, p2_tallies


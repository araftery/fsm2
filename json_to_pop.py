import json, utils, visualize_strategies

with open('player1_pop.json', 'r') as infile:
    player1_pop = json.load(infile)
    player1_pop = [utils.from_dict(player) for player in player1_pop]

with open('player2_pop.json', 'r') as infile:
    player2_pop = json.load(infile)
    player2_pop = [utils.from_dict(player) for player in player2_pop]

visualize_strategies.visualize(player1_pop, player2_pop)
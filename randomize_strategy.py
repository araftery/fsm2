import simulation

# probability that temptation is low
p = .51

# player1 payoff for cooperating
# c_h > c_l > a > 0
a = 1.0

# player1 payoffs for defecting
# c_l if temptation is low, c_h if temptation is high
c_l = 4.0
c_h = 12.0

# payoff for player2 if player1 cooperates
# b > 0
b = 1.0
d = -10.0

# probability that game repeats
w = .895

# population size
pop = 1000

# probability that any one player will be mutated each generation
# or, the expected percentage of the population that will be mutated each generation
mutate_prob = .2

generations = 50000
milestone = 5000
send_texts = False

player1_pop, player2_pop = simulation.run_simulation(p=p, a=a, c_l=c_l, c_h=c_h, b=b, d=d, w=w, pop=pop, mutate_prob=mutate_prob, generations=generations, milestone=milestone, send_texts=send_texts)
from twilio.rest import TwilioRestClient
import textwrap, os
def send_text(body):
    TWILIO_ACCOUNT_SID = os.environ.get('TWILIO_ACCOUNT_SID')
    TWILIO_AUTH_TOKEN = os.environ.get('TWILIO_AUTH_TOKEN')
    PHONE = os.environ.get('MY_PHONE')
    account_sid = TWILIO_ACCOUNT_SID
    auth_token  = TWILIO_AUTH_TOKEN
    client = TwilioRestClient(account_sid, auth_token)
    bodies = textwrap.wrap(body, 160)
    for body in bodies:
        message = client.sms.messages.create(
        body=body,
        to="+1{0}".format(PHONE),
        from_="+16172998450")